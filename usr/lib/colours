#!/bin/sh
#
# - colours
#
# library containing functions that mess with colours
#
# https://github.com/BanchouBoo/stain
#

hex2rgb() {
  # shellcheck disable=2046,2048,2086
  [ -t 0 ] || set -- $(cat) $*
  for i; do
    hex=${i#\#}
    r=${hex%%????}
    g=${hex##??}
    g=${g%%??}
    b=${hex##????}

    printf '%d %d %d\n' "0x$r" "0x$g" "0x$b"
  done
}

hexblk() {
  # shellcheck disable=2046,2048,2086
  [ -t 0 ] || set -- $(cat) $*
  for i; do
    # shellcheck disable=2046
    set -- $(hex2rgb "$i")
    printf '\033[48;2;%s;%s;%sm          \033[m #%s\n' "$1" "$2" "$3" "$i"
  done
}

invert() {
  # shellcheck disable=2046,2048,2086
  [ -t 0 ] || set -- $(cat) $*
  for i; do
    hex=${i#\#}
    # shellcheck disable=2046
    set -- $(hex2rgb "$hex")

    r=$(($1 * -1 + 255))
    g=$(($2 * -1 + 255))
    b=$(($3 * -1 + 255))
    printf '%02x%02x%02x\n' "$r" "$g" "$b"
  done
}

randcolour() {
  rand() {
    i=$1
    while [ "$i" -gt 0 ]; do
      r=$(tr -dc 1-9 </dev/urandom | dd ibs=1 obs=1 count=5 2>/dev/null)
      r=$((r % 255))
      printf '%s\n' "$r"
      i=$((i - 1))
    done
  }

  count=${1:-1}
  count=$((count * 3))
  rand "$count" | rgb2hex
}

rgb2hex() {
  # shellcheck disable=2046,2048,2086
  [ -t 0 ] || set -- $(cat) $*
  while [ "$1" ]; do
    printf '%02x%02x%02x\n' "$1" "$2" "$3"
    shift 3
  done
}
