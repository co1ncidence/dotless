#!/usr/bin/env zsh
#
# - zshrc
#
# zsh configuration file
#

# comments in shell
set -k
setopt INTERACTIVE_COMMENTS

# sources
source "$HOME/etc/zsh/.alii"
source "$HOME/etc/zsh/.high"
source "$HOME/etc/zsh/.func"

# libs
source "$HOME/usr/lib/colours"
source "$HOME/usr/lib/videos"
source "$HOME/usr/lib/images"
# source "$HOME/usr/lib/strings"
# source "$HOME/usr/lib/files"

# cd by typing dir name
setopt auto_cd
setopt autocd extendedglob nomatch notify

# completion settings
setopt NO_NOMATCH
setopt complete_in_word
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu select
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' matcher-list \
	'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
autoload -U compinit && compinit -C

# compinit
autoload -Uz compinit
compinit

# history settings
export HISTFILE="$HOME/etc/zsh/.histfile"
export HISTSIZE=42069
export SAVEHIST=42069
setopt hist_ignore_dups
setopt histignorespace
setopt share_history

# disable dumb stuff
unsetopt flowcontrol
unsetopt beep

# keybinds
bindkey '^a' beginning-of-line
bindkey '^e' end-of-line
bindkey '^w' backward-kill-word
bindkey '^f' forward-char
bindkey '^b' backward-char
bindkey '^r' history-incremental-search-backward

# keybinds
lu() { autoload -U "$1"; zle -N "$1"; bindkey "$2" "$1"; }

# ibid
lu up-line-or-beginning-search   '^[[A'
lu down-line-or-beginning-search '^[[B'
lu up-line-or-beginning-search   '^p'
lu down-line-or-beginning-search '^n'
lu edit-command-line             '^x'

# git status on ^g
kgs() { clear; git status -sb; zle redisplay }
zle -N kgs; bindkey '^g' kgs

# ls on ^k
kls() { clear; ls; zle redisplay }
zle -N kls; bindkey '^k' kls

# ^y for stuf
ktd() { nvim "$HOME/rey/stf"; zle redisplay }
zle -N ktd; bindkey '^y' ktd

# prompt (red on exit 1)
PROMPT=$'%F{255}%1/%f %B%(?.%F{0}.%F{1})%(!.#.~) %f%b'

# sudo pwd msg
export SUDO_PROMPT=$'pwd for\033[38;33'"${acc}m %u"$'\033[0m '

# cmd not found msg
command_not_found_handler() {
	echo -e "nope, \e[31m'$0'\e[0m didn't work."; return 1
}

# fzf
[ -f "${XDG_CONFIG_HOME:-$HOME/etc}"/fzf/fzf.zsh ] &&
	source "${XDG_CONFIG_HOME:-$HOME/etc}"/fzf/fzf.zsh

export FZF_DEFAULT_OPTS='
  --color fg:15,hl:1,hl+:1
  --color info:7,prompt:6,spinner:3,pointer:1,marker:4
	--reverse'

# set colourscheme
cat "$HOME/var/col/escape"
