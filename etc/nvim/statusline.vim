hi Sl1 ctermfg=15   ctermbg=NONE cterm=NONE
hi Sl2 ctermfg=8    ctermbg=NONE cterm=NONE
hi SlR ctermfg=NONE ctermbg=NONE cterm=NONE

function! SetModifiedSymbol(modified)
	if a:modified == 1
		return '* '
	else
		return ''
	endif
endfunction

set statusline=%#SlR#\ 
set statusline+=%=
set statusline+=%#SlR#
set statusline+=%#Sl2#\ %l,%c
set statusline+=%#Sl2#\ \ 
set statusline+=%#Sl1#\ %.80t\ 
set statusline+=%#Sl1#%{SetModifiedSymbol(&modified)}
set statusline+=%#SlR#\ 
