" Vim syntax file
" Language: Schedule
" Maintainer: nuaNce

if exists("b:current_syntax")
  finish
endif

syn match sctask '^\t[^\*.*].*'
syn match sctime '^\d*'
syn match sccomm ';.*'

hi def link sctask Normal
hi def link sctime Keyword
hi def link sccomm Comment

let b:current_syntax = "sch"
